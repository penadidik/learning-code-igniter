<?php
//Created by Didik Maryono (theapps.id) on May 2020
class M_Base extends CI_Model{
    
    //select table function
    function select($table){
        $this->db->select("*");
        $this->db->from($table);
        $this->db->order_by('id','DESC');

        return $this->db->get();
    }

    //select by
    function select_by($table,$where){
        return $this->db->get_where($table, $where);
    }

    //add data
    function add_data($table,$data){
        $this->db->insert($table, $data);

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    //change data
    function change_data($table,$data,$id){
        $this->db->update($table, $data, array('id'=>$id));

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    //delete data
    function delete_data($table,$id){
        $this->db->delete($table, array('id'=>$id));

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

 }