<?php
//Created by Didik Maryono (theapps.id) on May 2020
class M_User extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->load->model('M_Base');
    }

    //how much data user
    function num_row(){
        return $this->M_Base->select('user')->num_rows();
    }

    //show all data user
    function show(){
        return $this->M_Base->select('user')->result();
    }

    //select by id
    function select_by_id($id){
        return $this->M_Base->select_by('user',array('id'=>$id))->result();
    }

    //add data user
    function add($data){
        return $this->M_Base->add_data('user', $data);
    }

    //edit data user
    function edit($data,$id){
        return $this->M_Base->change_data('user',$data,$id);
    }

    //delete data user
    function delete($id){
        return $this->M_Base->delete_data('user',$id);
    }

}