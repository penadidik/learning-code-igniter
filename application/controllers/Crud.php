<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('M_User');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $data['user'] = $this->M_User->show();
		$this->load->view('hello', $data);
    }

    public function add_data()
	{
		$this->load->view('hello-tambah');
    }

    public function save_data(){
        $data = array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address')
        );

        if($this->M_User->add($data) == TRUE){
            redirect(base_url("index.php/crud/"));
        }else{
            redirect(base_url("index.php/crud/add_data"));
        }
    }

    public function edit_data($id){
        $data['user'] = $this->M_User->select_by_id($id);
        $this->load->view('hello-edit',$data);
    }

    public function save_edit(){
        $id = $this->input->post('id');
        $data = array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address')
        );

        if($this->M_User->edit($data,$id) == TRUE){
            redirect(base_url("index.php/crud/"));
        }else{
            redirect(base_url("index.php/crud/edit_data"));
        }
    }

    public function delete_data($id){

        if($this->M_User->delete($id) == TRUE){
            redirect(base_url("index.php/crud/"));
        }else{
            redirect(base_url("index.php/crud/"));
        }
    }
    
}
