<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head></head>
<body>
<h1>View Data</h1>
<a href="<?php echo base_url('index.php/crud'); ?>/add_data">Add Data</a>
<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Address</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($user as $show){ ?>
            <tr>
            <td><?php echo $show->id; ?></td>
            <td><?php echo $show->name; ?></td>
            <td><?php echo $show->address; ?></td>
            <td>
                <a href="<?php echo base_url('index.php/crud'); ?>/edit_data/<?php echo $show->id; ?>">Edit</a><br>
                <a href="<?php echo base_url('index.php/crud'); ?>/delete_data/<?php echo $show->id; ?>">Delete</a>
                </td>
        </tr>
        <?php } ?>
    </tbody>
</table>

</body>
</html>